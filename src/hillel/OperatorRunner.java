package hillel;

public class OperatorRunner {
    public static void main(String[] args) {

        int a = 1;

        //...

        a = ~a;

        System.out.println(Integer.toBinaryString(a));
        System.out.println(Integer.toBinaryString(~a));
        System.out.println(!true);
        System.out.println(4/2);
        System.out.println(4*2);
        System.out.println(4%2);
        System.out.println(4 + 7);
        System.out.println(4.0f/5.0f);

        int c = 1;

        int b = 2;

        System.out.println(c != b);

        System.out.println(true || false);

        System.out.println(true && false);

        System.out.println();

        b = a % 2 != 0 ? 5 : 17;

        a += 2;

       double result = Math.pow(25, 2);
        System.out.println(result);

        double res = Math.abs(-265);
        System.out.println(res);

        System.out.println(Math.sqrt(25));

        double random = Math.random();
        System.out.println((float)random * 100);

        System.out.println((2+3)*4);







    }
}
