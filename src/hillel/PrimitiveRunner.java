package hillel;

public class PrimitiveRunner {

    public static void main(String[] args) {

        boolean booleanVariable = true; //false

        byte byteVariable = 127;

        short shortValue = 1024;

        int integerValue = 9_000_000;

        char charVariable = '?';

        char anotherChar = 1280;

        long longValue = 90_000_000L;

        float floatValue = 365.78901f;

        double doubleValue = 356.0d;

        int binary = 0b001001;

        int octal = 0753;

        int hexadecimal = 0xFFE333;

        int bigBucket = 20000000;

        byte smallBucket = 125;

        smallBucket = (byte) bigBucket;


        System.out.println(binary);

        System.out.println(hexadecimal);

        System.out.println(smallBucket);

        System.out.println(bigBucket + " as binary: " + Integer.toBinaryString(bigBucket));
        System.out.println((byte) 0b11010110);








        }
    }
